import React from 'react';
import classes from './day.module.css';

const Day = props => {

  const { day, dayStyles, outOfMonthStyles, todayStyles, onClick } = props;

  let finalClassNames = `${classes.Day}`;

  let finalStyles = { ...dayStyles };

  if (day.events && day.events.length > 0) {
    finalStyles = {...finalStyles, backgroundColor: day.events[0].color}
  }

  if (day.outOfMonth) {
    finalStyles = {...finalStyles, ...outOfMonthStyles };
  }

  if (day.isToday) {
    finalClassNames = `${finalClassNames} ${classes.Today}`;
    finalStyles = { ...finalStyles, ...todayStyles };
  }

  return <div 
            className={finalClassNames} 
            style={finalStyles}
            title={day.title}
            onClick={() => onClick(day)}
            >
                {day.date.getDate()}
        </div>;
  
};

export default Day;