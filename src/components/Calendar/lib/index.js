
export const MONTH_NAMES = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre',
];

export const WEEK_NAMES = [
  'L',
  'M',
  'Mx',
  'J',
  'V',
  'S',
  'D',
];

const filterEventsByDate = (events, date) => {
  return events.filter(event => 
    event.date.getFullYear() === date.getFullYear() 
    && event.date.getMonth() === date.getMonth() 
    && event.date.getDate() === date.getDate());
} 

export const calculateCalendar = (date, events) => {

  const days = [];
  
  const year = date.getFullYear();
  const month = date.getMonth();

  let newDate = new Date(year, month, 1);

  // Push current month days
  while (newDate.getMonth() === month){

    const newDay = { date: new Date(newDate), outOfMonth: false, events: [], isToday: isToday(newDate) }

    const newDayEvents = filterEventsByDate(events, newDate);

    if (newDayEvents.length > 0) {
      newDayEvents.forEach(event => newDay.events.push(event))
    }

    days.push(newDay);
    newDate.setDate(newDate.getDate() + 1);
  }

  // Fill days before
  newDate = new Date(days[0].date);

  while(newDate.getDay() !== 1) {
    
    newDate.setDate(newDate.getDate() - 1);
    const copy = new Date(newDate);
    
    days.unshift({ date: copy, outOfMonth: true });
  }
  
  // Fill days after
  newDate = new Date(days[days.length - 1].date);

  while(newDate.getDay() !== 0) {

    newDate.setDate(newDate.getDate() + 1);
    const copy = new Date(newDate);

    days.push({ date: copy, outOfMonth: true });
  }

  // Group days in weeks
  const weeks = [];
  let newWeek = { week: 0, days: [] };

  for (let i = 0; i < days.length; i++) {

    newWeek.days.push(days[i]);

    if (newWeek.days.length === 7){
      weeks.push({...newWeek});
      newWeek = { week: newWeek.week + 1, days: [] };
    }

  }

  return weeks;
}

export const createMonths = (start, end) => {
  
  const range = [];

  let curDate = start;

  while (curDate < end){
    range.push(new Date(curDate));
    curDate = new Date(curDate.setMonth(curDate.getMonth() + 1))
  }

  return range;
}

export const isToday = (date) => {

  const today = new Date()

  return date.getFullYear() === today.getFullYear()
  && date.getMonth() === today.getMonth()
  && date.getDate() === today.getDate();
}