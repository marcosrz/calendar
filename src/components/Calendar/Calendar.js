import React from 'react';
import classes from './calendar.module.css';
import { MONTH_NAMES, WEEK_NAMES, calculateCalendar } from './lib';
import Day from './Day/Day';
import events from '../../assets/events';

const Calendar = (props) => {

  const { showTitle,
     title,
     date,
     onMonthClick,
     onDayClick,
     headerStyles,
     dayStyles,
     titleStyles,
     outOfMonthStyles,
     todayStyles } = props;

  const realWeeks = calculateCalendar(date, events);

  return (
      <div className={classes.Calendar}>

        {/* Title */}
        { showTitle ? 
          <h2 
            onClick={() => onMonthClick(date)}
            className={classes.CalendarTitle}
            style={titleStyles}>
              { title || `${MONTH_NAMES[date.getMonth()]}` }
          </h2> : null }

        {/* Week headers */}
        <div className={classes.Week}>
          {WEEK_NAMES.map(weekName => <div key={weekName} className={classes.DayHeader} style={headerStyles}>{weekName}</div>)}
        </div>

        {/* Weeks */}
        {
          realWeeks.map(week => (
            <div key={week.week} className={classes.Week}>
            {
              week.days.map(day => <Day 
                  key={`${week.week}_${day.date.getDate()}`}
                  day={day} 
                  dayStyles={dayStyles}
                  outOfMonthStyles={outOfMonthStyles}
                  todayStyles={todayStyles}
                  onClick={() => onDayClick(day)}
                />)
            }
            </div>)
          )
        }
      </div>
  );
};

export default Calendar;