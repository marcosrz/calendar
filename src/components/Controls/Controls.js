import React from 'react';
import classes from './controls.module.css';
const Controls = (props) => {

  const { onPreviousClick, onNextClick, year } = props;

  return (
    <div className={classes.Controls}>
    <button className={classes.Button} onClick={onPreviousClick}>{'<'}</button>
    <span className={classes.Label}>{year}</span>
    <button className={classes.Button} onClick={onNextClick}>{'>'}</button>
  </div>
  );
};

export default Controls;