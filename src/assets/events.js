export default [
  { 
    date: new Date(2020,  0, 1),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  0, 6),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  1, 24),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  1, 25),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  2, 16),
    title: 'Vacaciones 2019',
    color: '#00FF0066'
  },
  { 
    date: new Date(2020,  2, 23),
    title: 'Vacaciones 2019',
    color: '#00FF0066' 
  },
  { 
    date: new Date(2020,  3, 6),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  3, 7),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  3, 8),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  3, 9),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  3, 10),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  4, 1),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  5, 29),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  5, 30),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  6, 1),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  6, 2),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  6, 3),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  6, 30),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  6, 31),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 7),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  8, 8),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  8, 9),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 10),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 11),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 14),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 15),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 16),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 17),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 18),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 28),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 29),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  8, 30),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  9, 12),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  9, 13),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  9, 14),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  9, 15),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  9, 16),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  10, 2),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  10, 3),
    title: 'Vacaciones 2020',
    color: '#0000FF77' 
  },
  { 
    date: new Date(2020,  11, 7),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  11, 8),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  11, 24),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  11, 25),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  11, 28),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  11, 29),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  11, 30),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
  { 
    date: new Date(2020,  11, 31),
    title: 'Dia Indra',
    color: '#FF000077' 
  },
]