import React, { useState } from 'react';
import classes from './App.module.css';
import Calendar from './components/Calendar/Calendar';
import { createMonths } from './components/Calendar/lib'
import Controls from './components/Controls/Controls';

const titleStyles= { backgroundColor: '#FFFFFFCC', color: '#333'};
const headerStyles= { backgroundColor: '#FFFFFF77', color: 'white', textShadow: '0px 0px 5px black' };
const dayStyles= { backgroundColor: '#00000000', color: 'white', textShadow: '0px 0px 5px black' };
const outOfMonthStyles= { backgroundColor: '#FFFFFF00', color: 'white', textShadow: 'none', opacity: .3};
const todayStyles= { textShadow: '0px 0px 5px white', backgroundColor: '#FFFFFF33'};

function App() {

  const [year, setYear] = useState(2020);

  const dates = createMonths(new Date(year, 0, 1), new Date(year,11,31));

  const nextYear = () => {
    setYear(year + 1);
  }
  
  const previousYear = () => {
    setYear(year - 1);
  }

  const onDayClick = (day) => {
    console.log(day)

    if (day.events && day.events.length){
      alert(day.events.map(day => day.title).concat('\n'))
    }
  }

  const onMonthClick = (month) => {
    console.log(month)
  }

  return (
    <div className={classes.Container}>

      <div className={classes.Foreground}>

        <Controls onPreviousClick={previousYear} onNextClick={nextYear} year={year} />

        <div className={classes.Calendars}>
            { dates.map(date => (
              <div className={classes.CalendarContainer} key={`${date.toISOString()}`}>
                <Calendar 
                  showTitle
                  // title="Buatdefac"
                  // headerStyles={{ backgroundColor: '#004882EE', color: 'white' }}
                  // dayStyles={{ backgroundColor: '#0088ff44', color: 'white' }}
                  titleStyles={titleStyles}
                  headerStyles={headerStyles}
                  dayStyles={dayStyles}
                  outOfMonthStyles={outOfMonthStyles}
                  todayStyles={todayStyles}
                  date={date}
                  onMonthClick={onMonthClick}
                  onDayClick={onDayClick}/>
              </div>
            ))}
        </div>
      </div>
      
      <div className={classes.BackgroundDarkener}></div>
    
      <div className={classes.Footer}>
        <p>{ new Date().getFullYear() } © <a rel="noopener noreferrer" target="_blank" href="https://www.linkedin.com/in/marcosrz/">Marcos Rodríguez Martínez</a></p> 
        <p>🚀 Powered by <a rel="noopener noreferrer" target="_blank" href="https://reactjs.org/">React</a> | 🏭 Hosted by <a rel="noopener noreferrer" target="_blank" href="https://www.netlify.com/">Netlify</a></p>
      </div>
    </div>
  );
}

export default App;
